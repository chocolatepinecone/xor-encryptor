#include "pch.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

// Encrypt string function
string encryptChars(char s[], unsigned int length, string key)
{
	// Cannot encrypt if no key is given
	if (key.size() == 0) {
		// Throw error when encryption key is not set
		cout << "Encryption key is not set, so encryption cannot be done" << endl;
		throw logic_error("Encryption key is not set, so encryption cannot be done");
	}

	// Encrypt string with key
	string encryptedString;
	for (unsigned int i = 0; i < length; ++i) {
		// Use XOR operator to XOR each letter of the string with each letter of the key
		s[i] ^= key[i%key.size()];
		encryptedString += s[i];
	}
	return encryptedString;
}

// MAIN
int main() 
{
	string path, newFileName;

	// Ask for path to file
	cout << "Which file do you want to en/de-crypt?" << endl;
	cin >> path;

	// Open file and close it if found
	ifstream myfile;
	myfile.open(path, ios::binary);

	// Check if file is opened successfully
	if (myfile.is_open())
	{
		string key;

		// Ask for encryption key
		cout << "What encryption key do you want to use?" << endl;
		cin >> key;

		// Ask for new file name
		cout << "What's the new en/de-crypted file name? (en/de-crypted file contents will be copied to here)" << endl;
		cin >> newFileName;

		// Open new file for writing
		ofstream myNewfile;
		myNewfile.open(newFileName, ios::binary);

		if (myNewfile.is_open())
		{
			// Try to en/de-crypt
			try
			{
				// Allocate a char array for storing the files contents
				myfile.seekg(0, std::ios::end); // Go to the end of the file
				unsigned int length = myfile.tellg(); // Report location of the end (this is the length fo the file)
				myfile.seekg(0, std::ios::beg); // Go back to the beginning for reading
				std::unique_ptr<char> buffer(new char[length]); // Allocate memory for a buffer of appropriate size
				myfile.read(buffer.get(), length); // Read the whole file into the buffer

				myNewfile << encryptChars(buffer.get(), length, key);
			}
			catch (logic_error err)
			{
				cout << err.what() << endl;
			}

			myNewfile.close();
		}
		else
		{
			cout << "New file could not be created. Maybe running this tool with admin rights will solve this." << endl;
		}

		// Close the file
		myfile.close();
	}
	else 
	{
		cout << "File could not be opened. Did you fill in the file name correctly, including extension type?" << endl;
	}

	system("pause");

	return 0;
}